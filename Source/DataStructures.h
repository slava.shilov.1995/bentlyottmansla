#pragma once

#include <vector>
#include <random>
#include <string>
#include <sstream>
#include <limits>

#include "Splay.h"
#include "Avl.h"

using namespace std;

const double eps = 1e-9;

enum RunMode {
	GenerateData,
	RandomSolving,
	FileSolving
};

enum GeneratorMode {
	Uniform,
	FixedLength
};

RunMode runMode;
string filename;
GeneratorMode generatorMode;
float dataMaxValue;
int dataLinesNumber;
bool useGraphics;

double scrx, scrX, scry, scrY;
double scrXM, scrYM, scrSize;

void ResetSCR() {
	scrx = numeric_limits<int>::max();
	scrX = numeric_limits<int>::min();
	scry = numeric_limits<int>::max();
	scrY = numeric_limits<int>::min();
	scrXM = 1;
	scrYM = 1;
	scrSize = 700;
}

struct Dot {
	double x;
	double y;

	Dot(double x, double y) : x(x), y(y) {}
	Dot() {
		x = y = 0;
	}

	bool operator<(Dot& r) {
		if (abs(x - r.x) > eps) {
			return (x < r.x);
		} else {
			if (abs(y - r.y) < eps) {
				return false;
			} else {
				return (y < r.y);
			}
		}
	}

	bool operator>(Dot& r) {
		return (r < *this);
	}

	bool operator==(Dot& r) {
		return (abs(x - r.x) == 0 && abs(y - r.y) == 0);
	}

	friend std::ostream& operator<<(ostream& out, Dot& dot) {
		out << dot.x << " " << dot.y;
		return out;
	}
}; 


struct IntersectDot {
	Dot dot;
	vector<int> lines;
};

struct TPos;

struct Line {
	Dot p1;
	Dot p2;
	bool isVertical;
	AVL<TPos, int>::Node* tPointer;

	Line(double x1, double y1, double x2, double y2, int index) {
		p1 = Dot(x1, y1);
		p2 = Dot(x2, y2);
		isVertical = (abs(p1.x - p2.x) < eps);
		if (p1.x > p2.x) {
			swap(p1, p2);
		} else if (isVertical) {
			if (p1.y > p2.y) {
				swap(p1, p2);
			}
		}
		tPointer = nullptr;
	}

	double GetY(double x, double dflt) {
		if (isVertical) {
			if (abs(x - p1.x) < eps) {
				return dflt;
			} else if (x > p1.x) {
				return numeric_limits<double>::max();
			} else {
				return numeric_limits<double>::min();
			}
		}
		return ((x - p1.x) * (p2.y - p1.y)) / (p2.x - p1.x) + p1.y;
	}

	pair<bool, Dot> operator*(Line& r) {
		Dot p;
		if (isVertical) {
			p.x = p1.x;
			p.y = r.GetY(p.x, p1.y);
		} else if (r.isVertical) {
			p.x = r.p1.x;
			p.y = GetY(p.x, p1.y);
		} else {
			double A1 = p2.y - p1.y;
			double B1 = p1.x - p2.x;
			double C1 = -p1.x * (p2.y - p1.y) + p1.y * (p2.x - p1.x);

			double A2 = r.p2.y - r.p1.y;
			double B2 = r.p1.x - r.p2.x;
			double C2 = -r.p1.x * (r.p2.y - r.p1.y) + r.p1.y * (r.p2.x - r.p1.x);

			double d = A1 * B2 - B1 * A2;
			double dx = -C1 * B2 + B1 * C2;
			double dy = -A1 * C2 + C1 * A2;

			p.x = dx / d;
			p.y = dy / d;
		}
		double mx = p1.y, mn = p2.y, rmx = r.p1.y, rmn = r.p2.y;
		if (mn > mx) {
			swap(mx, mn);
		} 
		if (rmn > rmx) {
			swap(rmn, rmx);
		}
		if (p.x >= max(p1.x, r.p1.x) && p.x <= min(p2.x, r.p2.x)) {
			if (p.y >= mn && p.y >= rmn && p.y <= mx && p.y <= rmx) {
				return { true, p };
			}
		}
		return { false, p };
	}

	bool operator<(Line& r) {
		if (isVertical) {
			return false;
		} else if (r.isVertical) {
			return true;
		}
		return ((p2.y - p1.y) / (p2.x - p1.x) < (r.p2.y - r.p1.y) / (r.p2.x - r.p1.x));
	}
};

vector<Line> dataLines;
vector<IntersectDot> dataDots;
vector<IntersectDot> naiveDots;

struct TPos {
	double x;
	double y;
	int lineIndex;

	TPos(double x, double y, int lineIndex) : x(x), y(y), lineIndex(lineIndex) {};
	TPos() {
		x = -1;
		y = -1;
		lineIndex = -1;
	}

	bool operator==(TPos& r) {
		return (y == r.y && x == r.x && lineIndex == r.lineIndex);
		return (abs(y - r.y) < eps && abs(x - r.x) < eps && lineIndex == r.lineIndex);
	}

	bool _Comp(TPos& l, TPos& r) {
		if (abs(l.y - r.y) > eps) {
			return (l.y < r.y);
		} else {
			return (dataLines[l.lineIndex] < dataLines[r.lineIndex]);
		}
	}

	bool operator<(TPos& r) {
		r.x = x;
		r.y = dataLines[r.lineIndex].GetY(r.x, y);
		
		return _Comp(*this, r);
	}

	bool operator>(TPos& r) {
		r.x = x;
		r.y = dataLines[r.lineIndex].GetY(r.x, y);
		
		return _Comp(r, *this);
	}

	bool operator<<(TPos& r) {
		return _Comp(*this, r);
	}

	bool operator>>(TPos& r) {
		return _Comp(r, *this);
	}

	friend std::ostream& operator<<(ostream& out, TPos& dot) {
		out << dot.y << " " << dot.x << " " << dot.lineIndex;
		return out;
	}
};

struct Refs {
	vector<int> L;
	vector<int> R;
	vector<int> I;

	Refs(vector<int> _L, vector<int> _R, vector<int> _I) : L(_L), R(_R), I(_I) {}
	Refs() {};

	bool _Insert(int k, vector<int>& v) {
		for (int i = 0; i < v.size(); ++i) {
			if (v[i] == k) {
				return false;
			}
		}
		v.emplace_back(k);
		return true;
	}

	bool InsertL(int k) {
		return _Insert(k, L);
	}

	bool InsertR(int k) {
		return _Insert(k, R);
	}

	bool InsertI(int k) {
		return _Insert(k, I);
	}

	int Size() {
		return (L.size() + R.size() + I.size());
	}

	vector<int> Uniques() {
		vector<int> u;
		for (int i = 0; i < L.size(); ++i) {
			_Insert(L[i], u);
		}
		for (int i = 0; i < R.size(); ++i) {
			_Insert(R[i], u);
		}
		for (int i = 0; i < I.size(); ++i) {
			_Insert(I[i], u);
		}
		return u;
	}

	friend std::ostream& operator<<(ostream& out, Refs& ref) {
		out << ref.L.size() << " " << ref.R.size() << " " << ref.I.size();
		return out;
	}
};

vector<Line> GenerateLines(int numLines, double maxValue) {
	random_device device;
	mt19937 gen(device());
	vector<Line> lines;
	ResetSCR();
	switch (generatorMode) {
	case GeneratorMode::Uniform: {
		uniform_real_distribution<double> dis(0, maxValue);
		for (int i = 0; i < numLines; ++i) {
			double x1 = dis(gen);
			double x2 = dis(gen);
			double y1 = dis(gen);
			double y2 = dis(gen);
			if (abs(x1 - x2) < 1.) {
				x2 += 10;
			}
			lines.emplace_back(x1, y1, x2, y2, lines.size());
			scrx = min(scrx, min(x1, x2));
			scry = min(scry, min(y1, y2));
			scrX = max(scrX, max(x1, x2));
			scrY = max(scrY, max(y1, y2));
		}
	}
		break;
	case GeneratorMode::FixedLength: {
		double len = dataMaxValue / 20;
		uniform_real_distribution<double> dis(len, maxValue - len);
		uniform_real_distribution<double> ang(0, 2 * 3.14);
		for (int i = 0; i < numLines; ++i) {
			double x = dis(gen);
			double y = dis(gen);
			double angle = ang(gen);
			double x2 = x + cos(angle) * len;
			double y2 = y + sin(angle) * len;
			lines.emplace_back(x, y, x2, y2, lines.size());
			scrx = min(scrx, min(x, x2));
			scry = min(scry, min(y, y2));
			scrX = max(scrX, max(x, x2));
			scrY = max(scrY, max(y, y2));
		}
	}
		break;
	}
	scrXM = scrSize / (scrX - scrx);
	scrYM = scrSize / (scrY - scry);
	return lines;
}

void SaveLines(vector<Line>& lines, string filename) {
	ofstream file(filename);
	file.precision(numeric_limits<double>::digits10);
	if (file.is_open()) {
		file.clear();
		for (int i = 0; i < lines.size(); ++i) {
			file << lines[i].p1.x << " " << lines[i].p1.y << " " << lines[i].p2.x << " " << lines[i].p2.y << endl;
		}
	} else {
		cout << "Error: can't open " << filename << endl;
	}
}

void SaveDots(vector<IntersectDot>& dots, string inputFilename) {
	string fn = filename.substr(0, filename.size() - 3) + "sol";
	ofstream file(fn);
	if (file.is_open()) {
		file.precision(numeric_limits<double>::digits10);
		file.clear();
		for (int i = 0; i < dots.size(); ++i) {
			file << dots[i].dot.x << " " << dots[i].dot.y;
			for (int j = 0; j < dots[i].lines.size(); ++j) {
				file << " " << dots[i].lines[j];
			}
			file << endl;
		}
		file.close();
	} else {
		cout << "Error: can't write solution to " << fn << endl;
	}
}

vector<Line> LoadLines(string filename) {
	ifstream file(filename);
	if (file.is_open()) {
		ResetSCR();
		vector<Line> lines;
		double x1, y1, x2, y2;
		string line;
		while (getline(file, line)) {
			istringstream iss(line);
			iss >> x1 >> y1 >> x2 >> y2;
			lines.emplace_back(x1, y1, x2, y2, lines.size());
			scrx = min(scrx, min(x1, x2));
			scry = min(scry, min(y1, y2));
			scrX = max(scrX, max(x1, x2));
			scrY = max(scrY, max(y1, y2));
		}
		scrXM = scrSize / (scrX - scrx);
		scrYM = scrSize / (scrY - scry);
		return lines;
	} else {
		cout << "Error: can't open " << filename << endl;
	}
	return {};
}