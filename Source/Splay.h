#pragma once
#include <iostream>

using namespace std;

#define SP2 pair<Node*, Node*>

template <typename X, typename Y> 
struct Splay {

	struct Node {
		X x;
		Y y;

		Node * parent;
		Node * left;
		Node * right;

		Node(X x, Y y) {
			this->x = x;
			this->y = y;
			parent = nullptr;
			left = nullptr;
			right = nullptr;
		}

		~Node() {
			delete right;
			delete left;
		}

		Y& GetY() {
			return y;
		}

	};

	Node * root;

	Splay() {
		root = nullptr;
	}

	Splay(Node * root) {
		this->root = root;
	}

	~Splay() {
		delete root;
	}

	void _SetParent(Node * c, Node * p) {
		if (c != nullptr) {
			c->parent = p;
		}
	}

	void _KeepParent(Node * p) {
		_SetParent(p->left, p);
		_SetParent(p->right, p);
	}

	void _Rotate(Node * p, Node * c) {
		Node * g = p->parent;
		if (g != nullptr) {
			if (g->left == p) {
				g->left = c;
			} else {
				g->right = c;
			}
		}
		if (p->left == c) {
			p->left = c->right;
			c->right = p;
		} else {
			p->right = c->left;
			c->left = p;
		}
		_KeepParent(c);
		_KeepParent(p);
		c->parent = g;
	}

	Node * _Splay(Node * v) {
		if (v->parent == nullptr) {
			root = v;
			return v;
		}
		Node * p = v->parent;
		Node * g = p->parent;
		if (g == nullptr) {
			_Rotate(p, v);
			root = v;
			return v;
		} else {
			if ((g->left == p) == (p->left == v)) {
				_Rotate(g, p);
				_Rotate(p, v);
			} else {
				_Rotate(p, v);
				_Rotate(g, v);
			}
			return _Splay(v);
		}
	}

	Node * SplitPoint(Node * v, X x) {
		if (v == nullptr) {
			return nullptr;
		}
		if (x == v->x) {
			return _Splay(v);
		}
		if (x < v->x && v->left != nullptr) {
			return SplitPoint(v->left, x);
		}
		if (v->x < x && v->right != nullptr) {
			return SplitPoint(v->right, x);
		}
		return _Splay(v);
	}

	Node * Find(Node * v, X x) {
		if (v == nullptr) {
			return nullptr;
		}
		if (x == v->x) {
			return _Splay(v);
		}
		if (x < v->x && v->left) {
			return Find(v->left, x);
		}
		if (v->x < x && v->right) {
			return Find(v->right, x);
		}
		return nullptr;
	}

	SP2 Split(Node * v, X x) {
		if (v == nullptr) {
			return SP2(nullptr, nullptr);
		}
		v = SplitPoint(v, x);
		if (v->x == x) {
			_SetParent(v->left, nullptr);
			_SetParent(v->right, nullptr);
			return SP2(v->left, v->right);
		}
		if (v->x < x) {
			Node * r = v->right;
			v->right = nullptr;
			_SetParent(r, nullptr);
			return SP2(v, r);
		} else {
			Node * l = v->left;
			v->left = nullptr;
			_SetParent(l, nullptr);
			return SP2(l, v);
		}
	}

	Node* Insert(X x, Y y) {
		if (Find(root, x) != nullptr) return nullptr;
		SP2 p = Split(root, x);
		root = new Node(x, y);
		root->left = p.first;
		root->right = p.second;
		_KeepParent(root);
		return root;
	}

	Node * Merge(Node * l, Node * r) {
		if (r == nullptr) {
			return l;
		}
		if (l == nullptr) {
			return r;
		}
		r = SplitPoint(r, l->x);
		r->left = l;
		l->parent = r;
		return r;
	}

	void Remove(X x) {
		Node * v = Find(root, x);
		if (v) {
			_SetParent(v->left, nullptr);
			_SetParent(v->right, nullptr);
			root = Merge(v->left, v->right);
			v->left = v->right = nullptr;
			delete v;
		}
	}

	void Print() {
		cout << "Splay:" << endl;
		_Print(root, 0);
		cout << endl;
	}

	void _Print(Node * p, int level) {
		if (p) {
			_Print(p->right, level + 1);

			for (int i = 0; i < level; ++i) cout << "  ";
			cout << "(" << p->x << "; " << p->y << ")" << endl;
			_Print(p->left, level + 1);
		}
	}

	void Clear() {
		int k = 0;
		while (root) {
			Remove(root->x);
		}
	}

	Node * Min(Node* start) {
		Node * v = start;
		if (!v) {
			return nullptr;
		}
		while (v->left) {
			v = v->left;
		}
		return _Splay(v);
	}
};
