#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>

#include "DataStructures.h"
#include "Config.h"
#include "BentlyOttmanAlgorithm.h"
#include "NaiveAlgorithm.h"
#include "Visualisation.h"

using namespace std;

void main() {
	srand(time(NULL));
	ReadConfig();
	switch (runMode) {
	case RunMode::GenerateData:
		dataLines = GenerateLines(dataLinesNumber, dataMaxValue);
		SaveLines(dataLines, filename);
		break;
	case RunMode::FileSolving:
		dataLines = LoadLines(filename);
		dataDots = BentlyOttmanAlgorithm(dataLines);
		SaveDots(dataDots, filename);
		break;
	case RunMode::RandomSolving:
		dataLines = GenerateLines(dataLinesNumber, dataMaxValue);
		dataDots = BentlyOttmanAlgorithm(dataLines);
		break;
	default:
		RestoreConfig();
	}

	if (useGraphics || runMode == RunMode::RandomSolving) {
		RunVisualisation();
	}
	
	//cin.get();
}
