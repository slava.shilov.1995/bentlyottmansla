#pragma once
#include <iostream>

template <typename X, typename Y>
struct AVL {
	struct Node {
		X x;
		Y y;
		int height;

		Node * left;
		Node * right;
		Node * parent;

		Node(X x, Y y) : x(x), y(y) {
			height = 1;
			left = right = parent = nullptr;
		};

		Y& GetY() {
			return y;
		}
	};

	Node * root;
	Node * newNode;

	AVL() {
		root = nullptr;
		newNode = nullptr;
	}

	int _GetHeight(Node * v) {
		return (v ? v->height : 0);
	}

	int _GetFactor(Node * v) {
		return _GetHeight(v->right) - _GetHeight(v->left);
	}

	void _FixHeight(Node * v) {
		int l = _GetHeight(v->left);
		int r = _GetHeight(v->right);
		v->height = (l > r ? l : r) + 1;

		if (v->left) v->left->parent = v;
		if (v->right) v->right->parent = v;
	}

	Node * _RotateRight(Node * a) {
		Node * b = a->left;
		a->left = b->right; 
		b->right = a; 
		_FixHeight(a);
		_FixHeight(b);
		return b;
	}

	Node * _RotateLeft(Node * a) {
		Node * b = a->right;
		a->right = b->left;
		b->left = a;
		_FixHeight(a);
		_FixHeight(b);
		return b;
	}

	Node * _Balance(Node * v) {
		_FixHeight(v);
		if (_GetFactor(v) == 2) {
			if (_GetFactor(v->right) < 0) {
				v->right = _RotateRight(v->right);
			}
			return _RotateLeft(v);
		}
		if (_GetFactor(v) == -2) {
			if (_GetFactor(v->left) > 0) {
				v->left = _RotateLeft(v->left);
			}
			return _RotateRight(v);
		}
		return v;
	}

	Node * Find(Node * v, X x) {
		if (!v) {
			return nullptr;
		}
		if (x == v->x) {
			return v;
		}
		if (x < v->x) {
			return Find(v->left, x);
		}
		if (x > v->x) {
			return Find(v->right, x);
		}
		return nullptr;
	}

	Node * Insert(X x, Y y) {
		root = _Insert(root, x, y);
		root->parent = nullptr;
		return newNode;
	}

	Node * _Insert(Node * v, X x, Y y) {
		if (!v) {
			newNode = new Node(x, y);
			return newNode;
		}

		if (x < v->x) {
			v->left = _Insert(v->left, x, y);
		} else if (x > v->x) {
			v->right = _Insert(v->right, x, y);
		} else {
			newNode = v;
			return newNode;
		}
		
		return _Balance(v);
	}

	void Swap(Node* r, Node* l) {
		swap(r->x, l->x);
		swap(r->y, l->y);
	}

	Node * Min(Node* start) {
		Node * v = start;
		if (!v) {
			return nullptr;
		}
		while (v->left) {
			v = v->left;
		}
		return v;
	}

	Node * _RemoveMin(Node * v) {
		if (v->left == nullptr) {
			return v->right;
		}
		v->left = _RemoveMin(v->left);
		return _Balance(v);
	}

	void Remove(X x) {
		root = _Remove(root, x);
		if (root) {
			root->parent = nullptr;
		}
	}

	Node * _Remove(Node * v, X& x) {
		if (!v) {
			return nullptr;
		}
		if (x < v->x) {
			v->left = _Remove(v->left, x);
		} else if (x > v->x) {
			v->right = _Remove(v->right, x);
		} else {
			Node * q = v->left;
			Node * r = v->right;
			delete v;
			if (!r) {
				return q;
			}
			Node * m = Min(r);
			m->right = _RemoveMin(r);
			m->left = q;
			return _Balance(m);
		}
		return _Balance(v);
	}

	void Print() {
		cout.precision(numeric_limits<double>::digits10);
		if (root) {
			if (root->parent) cout << "WRONG ";
			cout << "AVL:" << endl;
			_Print(root, 0);
			cout << endl;
		}
	}

	void _Print(Node * p, int level) {
		if (p) {
			_Print(p->right, level + 1);

			for (int i = 0; i < level; ++i) cout << "  ";
			cout << "(" << p->x << ")" << endl;
			_Print(p->left, level + 1);
		} 
	}

	void _Parents(Node * p, int level) {
		if (p) {
			_Parents(p->right, level + 1);

			for (int i = 0; i < level; ++i) cout << "  ";
			cout << "(" << p->x.lineIndex << "; " << (p->parent?p->parent->x.lineIndex:-1) << ")" << endl;
			_Parents(p->left, level + 1);
		} 
	}

	Node * RightNeighbor(Node * v) {
		if (v->right) {
			Node * p = v->right;
			while (p->left) {
				p = p->left;
			}
			return p;
		} else {
			Node * p = v;
			while (p->parent && p->parent->right == p) {
				p = p->parent;
			}
			if (p->parent) {
				return p->parent;
			} else {
				return nullptr;
			}
		}
	}

	Node * LeftNeighbor(Node * v) {
		if (v->left) {
			Node * p = v->left;
			while (p->right) {
				p = p->right;
			}
			return p;
		} else {
			Node * p = v;
			while (p->parent && p->parent->left == p) {
				p = p->parent;
			}
			if (p->parent) {
				return p->parent;
			} else {
				return nullptr;
			}
		}
	}
};