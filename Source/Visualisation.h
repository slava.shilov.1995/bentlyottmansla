#pragma once
#include <chrono>

#include <SFML/Graphics.hpp>
#include "DataStructures.h"

class Timer {
	chrono::system_clock::time_point start, finish;

public:
	double duration;

	void Start() {
		start = chrono::system_clock::now();
	}

	void Finish() {
		finish = chrono::system_clock::now();
		duration = chrono::duration<double>(finish - start).count();
	}

	friend ostream& operator<<(ostream& out, Timer& r) {
		out << r.duration;
		return out;
	}
};

void RunVisualisation() {
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	static sf::RenderWindow window(sf::VideoMode(scrSize, scrSize), "", sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize, settings);

	sf::CircleShape dot;
	dot.setRadius(3);
	dot.setFillColor(sf::Color::Red);
	//dot.setOutlineThickness(1);
	dot.setOrigin(dot.getRadius(), dot.getRadius());

	sf::CircleShape naiveDot;
	naiveDot.setRadius(3);
	naiveDot.setFillColor(sf::Color::Blue);
	//dot.setOutlineThickness(1);
	naiveDot.setOrigin(naiveDot.getRadius(), naiveDot.getRadius());

	sf::Vertex line[] = {
		sf::Vertex(sf::Vector2f(0, 0)),
		sf::Vertex(sf::Vector2f(0, 0))
	};
	line[0].color = sf::Color::Green;
	line[1].color = sf::Color::Green;

	sf::Text text;
	text.setFillColor(sf::Color::White);
	text.setCharacterSize(16);
	sf::Font font;
	font.loadFromFile("Arial.ttf");
	text.setFont(font);

	bool newOne = false;
	Timer timer;

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape) {
					window.close();
				} else if (event.key.code == sf::Keyboard::Enter) {
					window.close();
				} else if (event.key.code == sf::Keyboard::R) {
					
					newOne = true;
				} else if (event.key.code == sf::Keyboard::T) {
					SaveLines(dataLines, "temp.txt");
					cout << "Line data saved to temp.txt" << endl;
				} else if (event.key.code == sf::Keyboard::S) {
					dataDots = BentlyOttmanAlgorithm(dataLines);
					naiveDots = NaiveAlgorithm(dataLines);
				} else if (event.key.code == sf::Keyboard::N) {
					dataDots = {};
					naiveDots = {};
				} else if (event.key.code == sf::Keyboard::O) {
					dataDots = {};
					naiveDots = {};
				} else if (event.key.code == sf::Keyboard::Q) {
					cout << "Qualification" << endl;
					for (int n = 1000; n <= 10000; n += 1000) {
						double duration = 0;
						for (int k = 0; k < 1; ++k) {
							dataLines = GenerateLines(n, dataMaxValue);
							timer.Start();
							dataDots = BentlyOttmanAlgorithm(dataLines);
							timer.Finish();
							duration += timer.duration;
						}
						duration /= 1;
						cout << duration << " ";
					}
					cout << endl;
					for (int n = 1000; n <= 10000; n += 1000) {
						double coef = 0;
						for (int k = 0; k < 1; ++k) {
							dataLines = GenerateLines(n, dataMaxValue);
							dataDots = BentlyOttmanAlgorithm(dataLines);
							coef += (double)dataDots.size() / dataLines.size();
						}
						coef /= 1;
						cout << coef << " ";
					}
				}
				break;
			}
		}

		if (newOne) {
			system("CLS");
			cout << "New one" << endl;
			dataLines = GenerateLines(dataLinesNumber, dataMaxValue);
		}

		window.clear();

		for (int i = 0; i < dataLines.size(); ++i) {
			line[0].position.x = (dataLines[i].p1.x - scrx) * scrXM;
			line[0].position.y = scrSize - (dataLines[i].p1.y - scry) * scrYM;
			line[1].position.x = (dataLines[i].p2.x - scrx) * scrXM;
			line[1].position.y = scrSize - (dataLines[i].p2.y - scry) * scrYM;
			
			if (dataLines[i].isVertical) {
				line[0].color = sf::Color::Yellow;
				line[1].color = sf::Color::Yellow;
			}
			window.draw(line, 2, sf::Lines);
			line[0].color = sf::Color::Green;
			line[1].color = sf::Color::Green;

			if (printConsole) {
				stringstream ss;
				ss << i;
				text.setString(ss.str().c_str());
				text.setPosition(line[0].position);
				window.draw(text);
			}

		}

		for (int i = 0; i < naiveDots.size(); ++i) {
			naiveDot.setPosition((naiveDots[i].dot.x - scrx) * scrXM, scrSize - (naiveDots[i].dot.y - scry) * scrYM);
			window.draw(naiveDot);
		}

		for (int i = 0; i < dataDots.size(); ++i) {
			dot.setPosition((dataDots[i].dot.x - scrx) * scrXM, scrSize - (dataDots[i].dot.y - scry) * scrYM);
			window.draw(dot);
		}

		window.display();

		if (newOne) {
			timer.Start();
			dataDots = BentlyOttmanAlgorithm(dataLines);
			timer.Finish();
			cout << "Time: " << timer << " " << (double)dataDots.size() / dataLines.size() << endl;

			timer.Start();
			naiveDots = NaiveAlgorithm(dataLines);
			timer.Finish();
			cout << "Time: " << timer << " " << (double)naiveDots.size() / dataLines.size() << endl;
			//if (dataDots.size() != naiveDots.size())
			newOne = false;
		}
	}

}