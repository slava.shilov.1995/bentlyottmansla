#pragma once

#include <vector>

using namespace std;

#define bts 5

template <typename X, typename Y>
struct BTree {
	struct Node {
		vector<pair<X, Y>> key;
		vector<Node*> link;
		int n;
		bool leaf;
		Node* parent;

		X x;
		Y y;
		int pos;

		Node() {
			key = vector<pair<X, Y>>(bts * 2 - 1);
			link = vector<Node*>(bts * 2, nullptr);
			n = 0;
			leaf = false;
			parent = nullptr;
		}

		Y& GetY() {
			return key[pos].second;
		}
	};

	Node* root = nullptr;
	Node* minNode = nullptr;

	Node* Find(Node* v, X x) {
		if (v) {
			int i;
			for (i = 0; i < v->n && v->key[i].first < x; ++i);
			if (i == v->n) {
				if (v->leaf) {
					return nullptr;
				} else {
					return Find(v->link[v->n], x);
				}
			} else {
				if (x == v->key[i].first) {
					v->x = x;
					v->y = v->key[i].second;
					v->pos = i;
					return v;
				} else if (!(v->leaf)) {
					return Find(v->link[i], x);
				} else {
					return nullptr;
				}
			}
			
		}
		return nullptr;
	}

	Node* _SplitIfNeed(Node* v, X x) {
		if (v->n == bts * 2 - 1) {
			Node* l = new Node();
			l->leaf = v->leaf;
			l->n = bts - 1;
			for (int j = 0; j < bts - 1; ++j) {
				l->key[j] = v->key[bts + j];
			}
			if (!(l->leaf)) {
				for (int j = 0; j < bts - 1; ++j) {
					l->link[j] = v->link[bts + j];
					l->link[j]->parent = l;
				}
				l->link[bts - 1] = v->link[bts * 2 - 1];
				l->link[bts - 1]->parent = l;
			}
			
			v->n = bts - 1;

			if (v->parent == nullptr) {
				Node* r = new Node();
				root = r;
				r->leaf = false;
				r->parent = nullptr;
				r->key[0] = v->key[bts - 1];
				r->n = 1;
				r->link[0] = v;
				r->link[1] = l;

				v->parent = r;
				l->parent = r;

				if (r->key[0].first < x) {
					v = l;
				}
			} else {
				Node* r = v->parent;
				int j = r->n;
				r->link[j + 1] = r->link[j];
				if (r->link[j] != v) {
					for (j = r->n - 1; j >= 0; --j) {
						r->key[j + 1] = r->key[j];
						r->link[j + 1] = r->link[j];
						if (r->link[j] == v) {
							break;
						}
					}
				}
				if (j < 0) j = 0;
				
				r->n++;
				r->key[j] = v->key[bts - 1];
				r->link[j + 1] = l;

				l->parent = r;

				if (r->key[j].first < x) {
					v = l;
				}
			}
		}
		return v;
	}

	Node* Insert(X x, Y y) {
		Node* v = root;
		if (!v) {
			root = new Node();
			minNode = root;
			root->leaf = true;
			root->parent = false;
			root->key[0] = { x, y };
			root->n = 1;
			return root;
		} else {
			while (true) {
				v = _SplitIfNeed(v, x);

				int i;
				for (i = 0; i < v->n && v->key[i].first < x; ++i);
				if (i == v->n) {
					if (!(v->leaf)) {
						v = v->link[i];
					} else {
						break;
					}
				} else {
					if (x == v->key[i].first) {
						return nullptr;
					} else if (!(v->leaf)) {
						v = v->link[i];
					} else {
						break;
					}
				}
			}

			int j;
			for (j = 0; j < v->n && v->key[j].first < x; ++j);
			if (j == v->n) {
				v->key[j] = { x, y };
				v->n++;
				return v;
			} else {
				if (x == v->key[j].first) {
					return nullptr;
				} else {
					for (int k = v->n - 1; k >= j; --k) {
						v->key[k + 1] = v->key[k];
					}
					v->key[j] = { x, y };
					v->n++;
					return v;
				}
			}
		}
	}

	int Count(Node* v) {
		int c = 0;
		if (v) {
			if (!(v->leaf)) {
				c += Count(v->link[v->n]);
				for (int i = v->n - 1; i >= 0; --i) {
					c += Count(v->link[i]);
				}
			}
			c += v->n;
		}
		return c;
	}

	Node* Min(Node* start) {
		if (minNode) {
			minNode->x = minNode->key[0].first;
			minNode->y = minNode->key[0].second;
			minNode->pos = 0;
			return minNode;
		}
		return nullptr;
	}

	void _RemoveMin(Node* v) {
		for (int i = 0; i < v->n - 1; ++i) {
			v->key[i] = v->key[i + 1];
			v->link[i] = v->link[i + 1];
		}
		v->link[v->n - 1] = v->link[v->n];
		v->n--;
	}

	void _RecursiveUpFix(Node* v) {
		if (v) {
			if (v->n < bts - 1) {
				if (v->parent) {
					Node* r = v->parent;
					Node* l = r->link[1];
					if (l->n > bts - 1) {
						v->key[v->n] = r->key[0];
						v->n++;
						if (!(v->leaf)) {
							v->link[v->n] = l->link[0];
							v->link[v->n]->parent = v;
						}
						r->key[0] = l->key[0];
						_RemoveMin(l);
					} else {
						v->key[v->n] = r->key[0];
						v->n++;
						if (!(l->leaf)) {
							for (int i = 0; i < l->n; ++i) {
								v->key[v->n] = l->key[i];
								v->link[v->n] = l->link[i];
								v->link[v->n]->parent = v;
								v->n++;
							}
							v->link[v->n] = l->link[l->n];
							v->link[v->n]->parent = v;
						} else {
							for (int i = 0; i < l->n; ++i) {
								v->key[v->n] = l->key[i];
								v->n++;
							}
						}
						
						delete l;
						_RemoveMin(r);
						r->link[0] = v;
						_RecursiveUpFix(r);
					}
				} else {
					if (v->n == 0) {
						if (!(v->leaf)) {
							root = v->link[0];
							root->parent = nullptr;
						} else {
							root = nullptr;
							minNode = nullptr;
						}
						delete v;
					}
				}
			}
		}
	}

	void Remove(X x) {
		Node* v = minNode;
		if (v) {
			_RemoveMin(v);
			_RecursiveUpFix(v);
		}

	}

	void _Print(Node* v, int level) {
		if (v) {
			if (!(v->leaf)) {
				_Print(v->link[v->n], level + 1);
				for (int i = v->n - 1; i >= 0; --i) {
					for (int k = 0; k < level; ++k) cout << "   ";
					cout << v->key[i].first << " " << v->key[i].second << endl;
					_Print(v->link[i], level + 1);
				}
			} else {
				for (int i = v->n - 1; i >= 0; --i) {
					for (int k = 0; k < level; ++k) cout << "   ";
					cout << v->key[i].first << " " << v->key[i].second << endl;
				}
			}
			
		}
	}

	void Print() {
		cout << "BTree:" << endl;
		_Print(root, 0);
	}
};