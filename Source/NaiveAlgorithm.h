#pragma once

#include "DataStructures.h"

vector<IntersectDot> NaiveAlgorithm(vector<Line> lines) {
	vector<IntersectDot> dots;
	for (int i = 0; i < lines.size() - 1; ++i) {
		for (int j = i + 1; j < lines.size(); ++j) {
			auto p = lines[i] * lines[j];
			if (p.first) {
				dots.push_back({ p.second, {i, j} });
			}
		}
	}
	return dots;
}