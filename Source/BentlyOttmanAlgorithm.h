#pragma once

#include "DataStructures.h"
#include "Splay.h"
#include "AVL.h"
#include "BTree.h"

const bool printConsole = false;

using QTree = BTree<Dot, Refs>;

pair<bool, Dot> FindIntersect(int sl, int su, Dot& q, vector<Line>& lines) {
	auto p = lines[sl] * lines[su];
	if (p.first) {
		if (q < p.second) {
			if (printConsole) cout << "cross " << sl << " " << su << endl;
			return { true, p.second };
		}
	}
	if (printConsole) cout << endl;
	return { false, p.second };
}

void S_S__(vector<Line>& lines, int i, int& s_, int& s__) {
	if (s_ == -1) {
		s_ = i;
	} else {
		if (lines[s_] < lines[i]) {
			s_ = i;
		}
	}
	if (s__ == -1) {
		s__ = i;
	} else {
		if (lines[i] < lines[s__]) {
			s__ = i;
		}
	}
}


void InsertT(Dot& dot, AVL<TPos, int>& T, vector<Line>& lines, vector<int> RLI, int& s_, int& s__) {
	for (int i = 0; i < RLI.size(); ++i) {
		if (lines[RLI[i]].tPointer == nullptr) {
			double y = lines[RLI[i]].GetY(dot.x, dot.y);
			lines[RLI[i]].tPointer = T.Insert({ dot.x, y, RLI[i] }, RLI[i]);
			if (printConsole) cout << "I insert " << RLI[i] << endl;
			S_S__(lines, RLI[i], s_, s__);
		}
	}
}

void ProcessPoint(QTree::Node* q, QTree& Q, AVL<TPos, int>& T, vector<IntersectDot>& intersectDots, vector<Line>& lines) {
	//1
	vector<int>& L = q->GetY().L;
	vector<int>& R = q->GetY().R;
	vector<int>& I = q->GetY().I;
	Dot dot = q->x;
	

	//2
	if (q->GetY().Size() > 1) {
		intersectDots.push_back({ dot, q->GetY().Uniques() });
	}
	//3
	if (printConsole) cout << "L " << L.size() << endl;
	if (printConsole) cout << "R " << R.size() << endl;
	if (printConsole) cout << "I " << I.size() << endl;
	//4
	int s_ = -1;
	int s__ = -1;
	if (printConsole) {
		cout << "T-";
		T.Print();
		T._Parents(T.root, 0);
	}

	if (I.size() > 1) {
		if (I.size() > 2) {
			for (int i = 0; i < I.size() - 1; ++i) {
				for (int j = i + 1; j < I.size(); ++j) {
					if (lines[I[i]] < lines[I[j]]) {
						swap(lines[I[i]], lines[I[j]]);
					}
				}
			}
			for (int i = 0, j = I.size() - 1; i < I.size() / 2; ++i, --j) {
				AVL<TPos, int>::Node* p1 = lines[I[i]].tPointer;
				AVL<TPos, int>::Node* p2 = lines[I[j]].tPointer;
				T.Swap(p1, p2);
				swap(lines[I[i]].tPointer, lines[I[j]].tPointer);
				p1->x.x = dot.x;
				p1->x.y = lines[p1->x.lineIndex].GetY(p1->x.x, dot.y);
				p2->x.x = dot.x;
				p2->x.y = lines[p2->x.lineIndex].GetY(p2->x.x, dot.y);
			}
		} else {
			AVL<TPos, int>::Node* p1 = lines[I[0]].tPointer;
			AVL<TPos, int>::Node* p2 = lines[I[1]].tPointer;
			T.Swap(p1, p2);
			swap(lines[I[0]].tPointer, lines[I[1]].tPointer);
			p1->x.x = dot.x;
			p1->x.y = lines[p1->x.lineIndex].GetY(p1->x.x, dot.y);
			p2->x.x = dot.x;
			p2->x.y = lines[p2->x.lineIndex].GetY(p2->x.x, dot.y);
		}
		
		for (int i = 0; i < I.size(); ++i) {
			S_S__(lines, I[i], s_, s__);
		}
	}
	
	//5
	
	for (int i = 0; i < R.size(); ++i) {
		if (lines[R[i]].tPointer != nullptr) {
			if (printConsole) cout << "I exit " << R[i] << endl;
			S_S__(lines, R[i], s_, s__);
		}
	}
	InsertT(dot, T, lines, L, s_, s__);

	if (printConsole) {
		cout << "T-";
		T.Print();
		T._Parents(T.root, 0);
	}

	
	
	//6
	AVL<TPos, int>::Node* su = T.RightNeighbor(lines[s_].tPointer);
	AVL<TPos, int>::Node* sl = T.LeftNeighbor(lines[s__].tPointer);
	if (printConsole) {
		if (su) cout << "su " << su->x.lineIndex << endl;
		cout << "s_ " << s_ << endl;
		cout << "s__ " << s__ << endl;
		if (sl) cout << "sl " << sl->x.lineIndex << endl;
	}
	if (su) {
		auto p = FindIntersect(su->x.lineIndex, s_, dot, lines);
		if (p.first) {
			auto v = Q.Find(Q.root, p.second);
			if (v) {
				v->GetY().InsertI(su->x.lineIndex);
				v->GetY().InsertI(s_);
			} else {
				Q.Insert(p.second, { {}, {}, {su->x.lineIndex, s_} });
			}
		}
	}
	if (sl) {
		auto p = FindIntersect(sl->x.lineIndex, s__, dot, lines);
		if (p.first) {
			auto v = Q.Find(Q.root, p.second);
			if (v) {
				v->GetY().InsertI(sl->x.lineIndex);
				v->GetY().InsertI(s__);
			} else {
				Q.Insert(p.second, { {}, {}, {sl->x.lineIndex, s__} });
			}
		}
	}

	for (int i = 0; i < R.size(); ++i) {
		AVL<TPos, int>::Node* su = T.RightNeighbor(lines[R[i]].tPointer);
		AVL<TPos, int>::Node* sl = T.LeftNeighbor(lines[R[i]].tPointer);
		if (printConsole) {
			if (su) cout << "su " << su->x.lineIndex << endl;
			cout << "s_ " << R[i] << endl;
			cout << "s__ " << R[i] << endl;
			if (sl) cout << "sl " << sl->x.lineIndex << endl;
		}
		if (su && sl) {
			auto p = FindIntersect(su->x.lineIndex, sl->x.lineIndex, dot, lines);
			if (p.first) {
				auto v = Q.Find(Q.root, p.second);
				if (v) {
					v->GetY().InsertI(su->x.lineIndex);
					v->GetY().InsertI(sl->x.lineIndex);
				} else {
					Q.Insert(p.second, { {}, {}, {su->x.lineIndex, sl->x.lineIndex} });
				}
			}
		}
		lines[R[i]].tPointer->x.x = dot.x;
		lines[R[i]].tPointer->x.y = lines[R[i]].GetY(dot.x, dot.y);
		T.Remove(lines[R[i]].tPointer->x);
		lines[R[i]].tPointer = nullptr;
	}
}

vector<IntersectDot> BentlyOttmanAlgorithm(vector<Line> lines) {
	QTree Q;
	AVL<TPos, int> T;
	vector<IntersectDot> intersectDots;

	QTree::Node* v;
	for (int i = 0; i < lines.size(); ++i) {

		v = Q.Find(Q.root, lines[i].p2);
		if (v) {
			v->GetY().InsertL(i);
		} else {
			Q.Insert(lines[i].p2, { {}, {i}, {} });
		}

		v = Q.Find(Q.root, lines[i].p1);
		if (v) {
			v->GetY().InsertL(i);
		} else {
			Q.Insert(lines[i].p1, { {i}, {}, {} });
		}

	}

	while (Q.root) {
		if (printConsole) cout << "Q-";
		if (printConsole) Q.Print();
		QTree::Node* q = Q.Min(Q.root);
		Dot x = q->x;
		if (printConsole) cout << q->x << endl << endl;
		
		ProcessPoint(q, Q, T, intersectDots, lines);

		Q.Remove(x);
		
		if (printConsole) cout << "#####" << endl;
	}

	return intersectDots;
}