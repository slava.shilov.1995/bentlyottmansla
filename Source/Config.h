#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "DataStructures.h"

using namespace std;

void RestoreConfig() {
	ofstream file("Config.txt");
	if (file.is_open()) {
		file.clear();
		
		file << "RunMode = G" << endl;
		file << "GeneratorMode = FL" << endl;
		file << "Filename = NewData.dat" << endl;
		file << "NewDataSize = 1000" << endl;
		file << "NewDataMaxValue = 800" << endl;
		file << "UseGraphics = 1" << endl;

		file << "//Please keep space around \" = \". It's not just an ini file." << endl;
		file << "//RunMode: G - Generate data and save it in \"NewData.dat\", R - generate random data and solve it, F - read data from File and solve it in <filename.sol>." << endl;
		file << "//GeneratorMode: U - uniform random lines, FL - fixed lenght lines" << endl;
		file << "//Filename: reading filename including format without \"\"." << endl;
		file << "//NewDataSize: number of generating lines." << endl;
		file << "//NewDataMaxValue: generating X and Y in range [0:NewDataMaxValue]." << endl;
		file << "//UseGraphics: 0 - fast mode without visualisation, 1 - use visualisation." << endl;

		file.close();

		cout << "Config file restored succesfully.";
	} else {
		cout << "Error: can't open config file to restore it." << endl;
	}
}

void ReadConfig() {
	ifstream file("Config.txt");
	bool allOk = true;
	if (file.is_open()) {
		try {
			string line;
			string defnull;
			//runMode
			getline(file, line);
			istringstream iss(line);
			string rm;
			iss >> defnull >> defnull >> rm;
			if (rm == "G") runMode = RunMode::GenerateData;
			else if (rm == "R") runMode = RunMode::RandomSolving;
			else if (rm == "F") runMode = RunMode::FileSolving;
			//generatorMode
			getline(file, line);
			iss = istringstream(line);
			string gm;
			iss >> defnull >> defnull >> gm;
			if (gm == "U") generatorMode = GeneratorMode::Uniform;
			else if (gm == "FL") generatorMode = GeneratorMode::FixedLength;
			//filename
			getline(file, line);
			iss = istringstream(line);
			iss >> defnull >> defnull >> filename;
			//dataMaxValue
			getline(file, line);
			iss = istringstream(line);
			iss >> defnull >> defnull >> dataLinesNumber;
			//dataLinesNumber
			getline(file, line);
			iss = istringstream(line);
			iss >> defnull >> defnull >> dataMaxValue;
			//useGraphics
			getline(file, line);
			iss = istringstream(line);
			iss >> defnull >> defnull >> useGraphics;
		} catch (...) {
			allOk = false;
		}
		file.close();
	} else {
		allOk = false;
	}
	if (!allOk) {
		cout << "Config file corrupted, closed or just dont exist. Restore Confix.txt to default? (y/n)" << endl;
		cout << "Answer: ";
		string answer;
		cin >> answer;
		cin.get();
		if (answer == "y") {
			RestoreConfig();
		}
	}

	cout << "RunMode: ";
	switch (runMode) {
	case RunMode::GenerateData:
		cout << "Data generation" << endl; 
		cout << "DataMaxValue: " << dataMaxValue << endl;
		cout << "DataLinesNumber: " << dataLinesNumber << endl;
		break;
	case RunMode::RandomSolving:
		cout << "Random data Solving" << endl;
		cout << "DataMaxValue: " << dataMaxValue << endl;
		cout << "DataLinesNumber: " << dataLinesNumber << endl;
		break;
	case RunMode::FileSolving:
		cout << "File data solving" << endl;
		cout << "Filename: " << filename << endl;
		break;
	}
	cout << "useGraphics: " << (useGraphics ? "true" : "false") << endl;
}

